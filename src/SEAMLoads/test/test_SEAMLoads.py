
import unittest
from SEAMLoads.SEAMLoads import SEAMLoads

def configure():

    top = SEAMLoads()

    top.hub_height = 100.0
    top.tsr = 8.0
    top.rotor_diameter = 101.0
    top.rated_power = 3.
    top.max_tipspeed = 62.
    top.n_wsp = 26
    top.min_wsp = 0.
    top.max_wsp = 25.
    top.turbulence_int = 0.16

    top.F = 0.777
    top.wohler_exponent_blade_flap = 10.0
    top.wohler_exponent_tower = 4.
    top.nSigma4fatFlap = 1.2
    top.nSigma4fatTower = 0.8
    top.dLoad_dU_factor_flap = 0.9
    top.dLoad_dU_factor_tower = 0.8
    top.lifetime_cycles = 1.e7
    top.blade_edge_dynload_factor_ext = 2.5
    top.blade_edge_dynload_factor_fat = 0.75

    top.WeibullInput = True
    top.weibull_C = 11.
    top.weibull_k = 2.00
    top.project_lifetime = 20.
    return top

class SEAMLoadsTestCase(unittest.TestCase):

    def setUp(self):

        self.top = configure()




    def test_loads(self):

        self.top.execute()
        self.assertAlmostEqual(self.top.blade_root_flap_max, 8721.38626352, places = 6)
        self.assertAlmostEqual(self.top.blade_root_edge_max, 4789.83725612, places = 6)
        self.assertAlmostEqual(self.top.tower_bottom_moment_max, 72924.5888233, places = 6)
        self.assertAlmostEqual(self.top.blade_root_flap_leq, 5218.66950788, places = 6)
        self.assertAlmostEqual(self.top.blade_root_edge_leq, 4347.64298655, places = 6)
        self.assertAlmostEqual(self.top.tower_bottom_moment_leq, 33688.5148707, places = 6)

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()
    #top = configure()
    #top.run()
